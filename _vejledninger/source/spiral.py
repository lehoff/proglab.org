import turtle


def spiral(length):
  if length > 5:
    turtle.forward(length)
    turtle.left(25)
    spiral(length*0.95)
